package client

import (
	"market_api_gateway/config"
	pbc "market_api_gateway/genproto/cash_service_protos"
	pbct "market_api_gateway/genproto/catalog_service_protos"
	pbo "market_api_gateway/genproto/organization_service_protos"
	pbw "market_api_gateway/genproto/warehouse_service_protos"

	"fmt"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	// Catalog service

	CategoryService() pbct.CategoryServiceClient
	ProductService() pbct.ProductServiceClient

	// Organization service

	AuthService() pbo.AuthServiceClient


	BranchService() pbo.BranchServiceClient
	BranchSellingPointService() pbo.BranchSellingPointServiceClient
	StaffService() pbo.StaffServiceClient
	ProviderService() pbo.ProviderServiceClient

	// Warehouse service

	IncomeService() pbw.IncomeServiceClient
	IncomeProductService() pbw.IncomeProductServiceClient
	ProductSellingService() pbw.ProductSellingServiceClient
	WarehouseProductService() pbw.WarehouseProductServiceClient

	// Cash service

	SaleService() pbc.SaleServiceClient
	SaleProductService() pbc.SaleProductServiceClient
	ShiftService() pbc.ShiftServiceClient
}

type grpcClients struct {
	// Catalog service

	categoryService pbct.CategoryServiceClient
	productService pbct.ProductServiceClient

	// Organization service

	authService pbo.AuthServiceClient

	branchService pbo.BranchServiceClient
	branchSellingPointService pbo.BranchSellingPointServiceClient
	staffService pbo.StaffServiceClient
	providerService pbo.ProviderServiceClient

	// Warehouse service

	incomeService pbw.IncomeServiceClient
	incomeProductService pbw.IncomeProductServiceClient
	productSellingService pbw.ProductSellingServiceClient
	warehouseProductService pbw.WarehouseProductServiceClient

	// Cash service

	saleService pbc.SaleServiceClient
	saleProductService pbc.SaleProductServiceClient
	shiftService pbc.ShiftServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connCatalogService, err := grpc.Dial(
		cfg.CatalogGrpcServiceHost+cfg.CatalogGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with catalog service client!",err.Error())
		return nil, err
	}

	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationGrpcServiceHost+cfg.OrganizationGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with organization service client!",err.Error())
		return nil, err
	}

	connWarehouseService, err := grpc.Dial(
		cfg.WarehouseGrpcServiceHost+cfg.WarehouseGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with warehouse service client!",err.Error())
		return nil, err
	}

	connCashService, err := grpc.Dial(
		cfg.CashGrpcServiceHost+cfg.CashGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with warehouse service client!",err.Error())
		return nil, err
	}

	return &grpcClients{
		categoryService: pbct.NewCategoryServiceClient(connCatalogService),
		productService: pbct.NewProductServiceClient(connCatalogService),

		authService: pbo.NewAuthServiceClient(connOrganizationService),

		branchService: pbo.NewBranchServiceClient(connOrganizationService),
		branchSellingPointService: pbo.NewBranchSellingPointServiceClient(connOrganizationService),
		staffService: pbo.NewStaffServiceClient(connOrganizationService),
		providerService: pbo.NewProviderServiceClient(connOrganizationService),

		incomeService: pbw.NewIncomeServiceClient(connWarehouseService),
		incomeProductService: pbw.NewIncomeProductServiceClient(connWarehouseService),
		productSellingService: pbw.NewProductSellingServiceClient(connWarehouseService),
		warehouseProductService: pbw.NewWarehouseProductServiceClient(connWarehouseService),

		saleService: pbc.NewSaleServiceClient(connCashService),
		saleProductService: pbc.NewSaleProductServiceClient(connCashService),
		shiftService: pbc.NewShiftServiceClient(connCashService),

	}, nil
}

// Catalog service

func (g *grpcClients) CategoryService() pbct.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() pbct.ProductServiceClient{
	return g.productService
}

// Organization service 

func (g *grpcClients) BranchService() pbo.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) BranchSellingPointService() pbo.BranchSellingPointServiceClient{
	return g.branchSellingPointService
}

func (g *grpcClients) StaffService() pbo.StaffServiceClient {
	return g.staffService
}

func (g *grpcClients) ProviderService() pbo.ProviderServiceClient{
	return g.providerService
}

func (g *grpcClients) AuthService() pbo.AuthServiceClient {
	return g.authService
}

// Warehouse service

func (g *grpcClients) IncomeService() pbw.IncomeServiceClient {
	return g.incomeService
}

func (g *grpcClients) IncomeProductService() pbw.IncomeProductServiceClient{
	return g.incomeProductService
}

func (g *grpcClients) ProductSellingService() pbw.ProductSellingServiceClient {
	return g.productSellingService
}

func (g *grpcClients) WarehouseProductService() pbw.WarehouseProductServiceClient{
	return g.warehouseProductService
}
 
// Cash service

func (g *grpcClients) SaleService() pbc.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() pbc.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) ShiftService() pbc.ShiftServiceClient {
	return g.shiftService
}
