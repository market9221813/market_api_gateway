package handler

import (
	pbc "market_api_gateway/genproto/cash_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateSaleProduct godoc
// @Router       /sale_product [POST]
// @Summary      Create a new sale_product
// @Description  Create a new sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        sale_product body models.CreateSaleProduct false "sale_product"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSaleProduct(c *gin.Context) {
	request := pbc.CreateSaleProductRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleProductService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating sale_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "SaleProduct Created!", http.StatusCreated, resp)
}

// GetSaleProduct godoc
// @Router       /sale_product/{id} [GET]
// @Summary      Gets sale_product
// @Description  get sale_product by id
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_product"
// @Success      200  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleProduct(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	saleProduct, err := h.services.SaleProductService().Get(ctx, &pbc.SaleProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting sale_product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, saleProduct)
}

// GetSaleProductList godoc
// @Router       /sale_products [GET]
// @Summary      Get sale_product list
// @Description  get sale_product list
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.SaleProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleProductList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleProductService().GetList(ctx, &pbc.GetSaleProductListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
 	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting sale_products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateSaleProduct godoc
// @Router       /sale_product/{id} [PUT]
// @Summary      Update sale_product
// @Description  update sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_product_id"
// @Param        sale_product body models.UpdateSaleProduct true "sale_product"
// @Success      200  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSaleProduct(c *gin.Context) {
	updateProduct := &pbc.SaleProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateProduct.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleProductService().Update(ctx, updateProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating sale_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteSaleProduct godoc
// @Router       /sale_product/{id} [DELETE]
// @Summary      Delete sale_product
// @Description  delete sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_product_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSaleProduct(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.SaleProductService().Delete(ctx, &pbc.SaleProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting sale_product!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
