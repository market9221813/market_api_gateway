package handler

import (
	"market_api_gateway/api/models"
	"market_api_gateway/config"
	pbu "market_api_gateway/genproto/organization_service_protos"
	"market_api_gateway/pkg/jwt"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
 )

// StaffLogin godoc
// @Router       /staff_login [POST]
// @Summary      Staff Login
// @Description  Staff Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.UserLoginRequest false "login"
// @Success      201  {object}  models.UserLoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) StaffLogin(c *gin.Context) {
	var (
		loginRequest  = pbu.UserLoginRequest{}
		loginResponse = models.UserLoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().UserLogin(ctx, &loginRequest)
	if err != nil {
		handleResponse(c,  "Error while getting client credentials!", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id": resp.GetId(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken

	handleResponse(c, "", http.StatusOK, loginResponse)
}

func (h Handler) GetAuthInfoFromToken(c *gin.Context) (models.AuthInfo, error) {
	accessToken := c.GetHeader("Authorization")
	if accessToken == "" {
 		return models.AuthInfo{}, config.ErrUnauthorized
	}

	claims, err := jwt.ExtractClaims(accessToken, string(config.SignKey))
	if err != nil {
		return models.AuthInfo{}, err
	}

	userID := cast.ToString(claims["user_id"])

	return models.AuthInfo{
		UserID: userID,
	}, nil
}

