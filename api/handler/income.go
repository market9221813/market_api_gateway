package handler

import (
	"market_api_gateway/config"
	organization_service "market_api_gateway/genproto/organization_service_protos"
	pbw "market_api_gateway/genproto/warehouse_service_protos"

	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateIncome godoc
// @Router       /income [POST]
// @Summary      Create a new income
// @Description  Create a new income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        income body models.CreateIncome false "income"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncome(c *gin.Context) {
	request := pbw.CreateIncomeRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating income", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Income created!", http.StatusCreated, resp)
}

// GetIncome godoc
// @Router       /income/{id} [GET]
// @Summary      Gets income
// @Description  get income by id
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income"
// @Success      200  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncome(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	income, err := h.services.IncomeService().Get(ctx, &pbw.IncomePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting income by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, income)
}

// GetIncomeList godoc
// @Router       /incomes [GET]
// @Summary      Get income list
// @Description  get income list
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.IncomesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeService().GetList(ctx, &pbw.GetIncomeListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting incomes", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateIncome godoc
// @Router       /income/{id} [PUT]
// @Summary      Update income
// @Description  update income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Param        income body models.UpdateIncome true "income"
// @Success      200  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncome(c *gin.Context) {
	updateIncome := &pbw.Income{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateIncome)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateIncome.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeService().Update(ctx, updateIncome)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating income", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteIncome godoc
// @Router       /income/{id} [DELETE]
// @Summary      Delete income
// @Description  delete income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncome(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.IncomeService().Delete(ctx, &pbw.IncomePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting income!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}

// MakeIncomeLogic godoc
// @Security ApiKeyAuth
// @Router       /income_logic [POST]
// @Summary      Make a new income product with logics
// @Description  Make a new income product with logics
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        income_product body models.CreateIncomeProduct false "income_product"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) MakeIncomeLogic(c *gin.Context) {
	staffResp, err := h.GetAuthInfoFromToken(c)
	if err != nil{
		handleResponse(c, "Error Unauthorized!",http.StatusUnauthorized, config.ErrUnauthorized)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	staff, err := h.services.StaffService().Get(ctx, &organization_service.StaffPrimaryKey{Id: staffResp.UserID})
	if err != nil{
		handleResponse(c,"Error while getting staff!", http.StatusInternalServerError,err)
	}

	if staff.Type == "storekeeper"{
		handleResponse(c,"Don't have an access!", http.StatusBadRequest, config.ErrUnauthorized)
		return
	}

	request := pbw.CreateIncomeProductRequest{}

	err = c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

 

	resp, err := h.services.IncomeProductService().MakeIncome(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating income_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Income Product created!", http.StatusCreated, resp)
}

// FinishIncome godoc
// @Router       /finish_income/{id} [PATCH]
// @Summary      Update income status to comleted
// @Description  update income status to completed
// @Tags         income
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) FinishIncome(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}


	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.IncomeService().FinishIncome(ctx, &pbw.IncomePrimaryKey{Id: id})
	if err != nil {
		handleResponse(c, "Error in handlers, while updating income", http.StatusInternalServerError, err.Error())
		return
	}

	msg := "The income is completed!"
	handleResponse(c, "Updated successfully", http.StatusOK, msg)
}