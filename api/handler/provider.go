package handler

import (
	pbo "market_api_gateway/genproto/organization_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateProvider godoc
// @Router       /provider [POST]
// @Summary      Create a new provider
// @Description  Create a new provider
// @Tags         provider
// @Accept       json
// @Produce      json
// @Param        provider body models.CreateProvider false "provider"
// @Success      201  {object}  models.Provider
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProvider(c *gin.Context) {
	request := pbo.CreateProviderRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProviderService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating provider", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "provider created!", http.StatusCreated, resp)
}

// GetProvider godoc
// @Router       /provider/{id} [GET]
// @Summary      Gets provider
// @Description  get provider by id
// @Tags         provider
// @Accept       json
// @Produce      json
// @Param        id path string true "provider"
// @Success      200  {object}  models.Provider
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProvider(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	provider, err := h.services.ProviderService().Get(ctx, &pbo.ProviderPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting provider by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, provider)
}

// GetProviderList godoc
// @Router       /providers [GET]
// @Summary      Get provider list
// @Description  get provider list
// @Tags         provider
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.ProvidersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProviderList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProviderService().GetList(ctx, &pbo.GetProviderListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting providers", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateProvider godoc
// @Router       /provider/{id} [PUT]
// @Summary      Update provider
// @Description  update provider
// @Tags         provider
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param        provider body models.UpdateProvider true "provider"
// @Success      200  {object}  models.Provider
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProvider(c *gin.Context) {
	updateProvider := &pbo.Provider{}

	id := c.Param("id")
	if id == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateProvider)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateProvider.Id = id

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProviderService().Update(ctx, updateProvider)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating provider", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteProvider godoc
// @Router       /provider/{id} [DELETE]
// @Summary      Delete provider
// @Description  delete provider
// @Tags         provider
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProvider(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.ProviderService().Delete(ctx, &pbo.ProviderPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting provider!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
