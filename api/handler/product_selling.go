package handler

import (
	pbw "market_api_gateway/genproto/warehouse_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateProductSelling godoc
// @Router       /product_selling [POST]
// @Summary      Create a new product_selling
// @Description  Create a new product_selling
// @Tags         product_selling
// @Accept       json
// @Produce      json
// @Param        product_selling body models.CreateProductSelling false "product_selling"
// @Success      201  {object}  models.ProductSelling
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProductSelling(c *gin.Context) {
	request := pbw.CreateProductSellingRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductSellingService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating product_selling", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "ProductSelling created!", http.StatusCreated, resp)
}

// GetProductSelling godoc
// @Router       /product_selling/{id} [GET]
// @Summary      Gets product_selling
// @Description  get product_selling by id
// @Tags         product_selling
// @Accept       json
// @Produce      json
// @Param        id path string true "product_selling"
// @Success      200  {object}  models.ProductSelling
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductSelling(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	productSelling, err := h.services.ProductSellingService().Get(ctx, &pbw.ProductSellingPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting product_selling by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, productSelling)
}

// GetProductSellingList godoc
// @Router       /product_sellings [GET]
// @Summary      Get product_selling list
// @Description  get product_selling list
// @Tags         product_selling
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.ProductSellingsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductSellingList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductSellingService().GetList(ctx, &pbw.GetProductSellingListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
 	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting product_selling", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateProductSelling godoc
// @Router       /product_selling/{id} [PUT]
// @Summary      Update product_selling
// @Description  update product_selling
// @Tags         product_selling
// @Accept       json
// @Produce      json
// @Param 		 id path string true "product_selling_id"
// @Param        product_selling body models.UpdateProductSelling true "product_selling"
// @Success      200  {object}  models.ProductSelling
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProductSelling(c *gin.Context) {
	updateProductSelling := &pbw.ProductSelling{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateProductSelling)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateProductSelling.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductSellingService().Update(ctx, updateProductSelling)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating product_selling", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteProductSelling godoc
// @Router       /product_selling/{id} [DELETE]
// @Summary      Delete product_selling
// @Description  delete product_selling
// @Tags         product_selling
// @Accept       json
// @Produce      json
// @Param 		 id path string true "product_selling_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProductSelling(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.ProductSellingService().Delete(ctx, &pbw.ProductSellingPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting product_selling!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
