package handler

import (
	pbc "market_api_gateway/genproto/cash_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateShift godoc
// @Router       /shift [POST]
// @Summary      Create a new shift
// @Description  Create a new shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        shift body models.CreateShift false "shift"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateShift(c *gin.Context) {
	request := pbc.CreateShiftRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ShiftService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating shift", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "shift created!", http.StatusCreated, resp)
}

// GetShift godoc
// @Router       /shift/{id} [GET]
// @Summary      Gets shift
// @Description  get shift by id
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift"
// @Success      200  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetShift(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	shift, err := h.services.ShiftService().Get(ctx, &pbc.ShiftPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting shift by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, shift)
}

// GetShiftList godoc
// @Router       /shifts [GET]
// @Summary      Get shift list
// @Description  get shift list
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.ShiftsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetShiftList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ShiftService().GetList(ctx, &pbc.GetShiftListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting shifts", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateShift godoc
// @Router       /shift/{id} [PUT]
// @Summary      Update shift
// @Description  update shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param 		 id path string true "shift_id"
// @Param        shift body models.UpdateShift true "shift"
// @Success      200  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateShift(c *gin.Context) {
	updateShift := &pbc.Shift{}

	id := c.Param("id")
	if id == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateShift)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateShift.Id = id

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ShiftService().Update(ctx, updateShift)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating shift", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteShift godoc
// @Router       /shift/{id} [DELETE]
// @Summary      Delete shift
// @Description  delete shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param 		 id path string true "shift_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteShift(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.ShiftService().Delete(ctx, &pbc.ShiftPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting shift!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}

// OpenShiftLogic godoc
// @Router       /open_shift [POST]
// @Summary      Opening a new shift
// @Description  Opening a new shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        shift body models.CreateShift false "shift"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) OpenShiftLogic(c *gin.Context) {
	request := pbc.CreateShiftRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ShiftService().OpenShift(ctx,&request)
	if err != nil {
		msg := "The branch has an opened shift, please close it before opening a new!"
		handleResponse(c,"Error in handlers, while creating shift", http.StatusInternalServerError,  msg)
		return
	}

	handleResponse(c, "shift created!", http.StatusCreated, resp)
}

// CloseShift godoc
// @Router       /close_shift/{id} [PATCH]
// @Summary      Close shift
// @Description  Close shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CloseShift(c *gin.Context) {

	id := c.Param("id")
	if id == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.ShiftService().CloseShift(ctx, &pbc.ShiftPrimaryKey{Id: id})
	if err != nil {
		handleResponse(c, "Error in handlers, while updating shift", http.StatusInternalServerError, err.Error())
		return
	}
	msg := "Kassa is succesfully closed" 

	handleResponse(c, "Updated successfully", http.StatusOK, msg)
}
