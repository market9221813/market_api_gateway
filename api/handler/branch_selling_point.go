package handler

import (
	pbo "market_api_gateway/genproto/organization_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateBranchSellingPoint godoc
// @Router       /branch_selling_point [POST]
// @Summary      Create a new branch_selling_point
// @Description  Create a new branch_selling_point
// @Tags         branch_selling_point
// @Accept       json
// @Produce      json
// @Param        branch_selling_point body models.CreateBranchSellingPoint false "branch_selling_point"
// @Success      201  {object}  models.BranchSellingPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranchSellingPoint(c *gin.Context) {
	request := pbo.CreateBranchSellingPointRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.BranchSellingPointService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating branch_selling_point", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "branch_selling_point created!", http.StatusCreated, resp)
}

// GetBranchSellingPoint godoc
// @Router       /branch_selling_point/{id} [GET]
// @Summary      Gets branch_selling_point
// @Description  get branch_selling_point by id
// @Tags         branch_selling_point
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_selling_point"
// @Success      200  {object}  models.BranchSellingPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchSellingPoint(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	branch_selling_point, err := h.services.BranchSellingPointService().Get(ctx, &pbo.BranchSellingPointPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting branch_selling_point by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, branch_selling_point)
}

// GetBranchSellingPointList godoc
// @Router       /branch_selling_points [GET]
// @Summary      Get branch_selling_point list
// @Description  get branch_selling_point list
// @Tags         branch_selling_point
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.BranchSellingPointsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchSellingPointList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.BranchSellingPointService().GetList(ctx, &pbo.GetBranchSellingPointListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting branch_selling_point", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateBranchSellingPoint godoc
// @Router       /branch_selling_point/{id} [PUT]
// @Summary      Update branch_selling_point
// @Description  update branch_selling_point
// @Tags         branch_selling_point
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param        branch_selling_point body models.UpdateBranchSellingPoint true "branch_selling_point"
// @Success      200  {object}  models.BranchSellingPoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranchSellingPoint(c *gin.Context) {
	updateBranch := &pbo.BranchSellingPoint{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateBranch)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateBranch.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.BranchSellingPointService().Update(ctx, updateBranch)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating branch_selling_point", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteBranchSellingPoint godoc
// @Router       /branch_selling_point/{id} [DELETE]
// @Summary      Delete branch_selling_point
// @Description  delete branch_selling_point
// @Tags         branch_selling_point
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranchSellingPoint(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.BranchSellingPointService().Delete(ctx, &pbo.BranchSellingPointPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting branch_selling_point!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
