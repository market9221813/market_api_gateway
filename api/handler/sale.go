package handler

import (
	pbc "market_api_gateway/genproto/cash_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateSale godoc
// @Router       /sale [POST]
// @Summary      Create a new sale
// @Description  Create a new sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        sale body models.CreateSale false "sale"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSale(c *gin.Context) {
	request := pbc.CreateSaleRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating sale", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "sale created!", http.StatusCreated, resp)
}

// GetSale godoc
// @Router       /sale/{id} [GET]
// @Summary      Gets sale
// @Description  get sale by id
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale"
// @Success      200  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSale(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	sale, err := h.services.SaleService().Get(ctx, &pbc.SalePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting sale by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, sale)
}

// GetSaleList godoc
// @Router       /sales [GET]
// @Summary      Get sale list
// @Description  get sale list
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.SalesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleService().GetList(ctx, &pbc.GetSaleListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting sales", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateSale godoc
// @Router       /sale/{id} [PUT]
// @Summary      Update sale
// @Description  update sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_id"
// @Param        sale body models.UpdateSale true "sale"
// @Success      200  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSale(c *gin.Context) {
	updateSale := &pbc.Sale{}

	id := c.Param("id")
	if id == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateSale)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateSale.Id = id

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.SaleService().Update(ctx, updateSale)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating sale", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteSale godoc
// @Router       /sale/{id} [DELETE]
// @Summary      Delete sale
// @Description  delete sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param 		 id path string true "sale_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSale(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.SaleService().Delete(ctx, &pbc.SalePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting sale!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
