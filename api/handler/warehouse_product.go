package handler

import (
	pbw "market_api_gateway/genproto/warehouse_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateWarehouseProduct godoc
// @Router       /warehouse_product [POST]
// @Summary      Create a new warehouse_product
// @Description  Create a new warehouse_product
// @Tags         warehouse_product
// @Accept       json
// @Produce      json
// @Param        warehouse_product body models.CreateWarehouseProduct false "warehouse_product"
// @Success      201  {object}  models.WarehouseProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateWarehouseProduct(c *gin.Context) {
	request := pbw.CreateWarehouseProductRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.WarehouseProductService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating warehouse_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "WarehouseProduct created!", http.StatusCreated, resp)
}

// GetWarehouse godoc
// @Router       /warehouse_product/{id} [GET]
// @Summary      Gets warehouse_product
// @Description  get warehouse_product by id
// @Tags         warehouse_product
// @Accept       json
// @Produce      json
// @Param        id path string true "warehouse_product"
// @Success      200  {object}  models.WarehouseProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetWarehouse(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	warehouse_product, err := h.services.WarehouseProductService().Get(ctx, &pbw.WarehouseProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting warehouse_product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, warehouse_product)
}

// GetWarehouseList godoc
// @Router       /warehouse_products [GET]
// @Summary      Get warehouse_product list
// @Description  get warehouse_product list
// @Tags         warehouse_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.WarehouseProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetWarehouseList(c *gin.Context) {
	var (
		page, limit int
 		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.WarehouseProductService().GetList(ctx, &pbw.GetWarehouseProductListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
 	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting warehouse_products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateWarehouseProduct godoc
// @Router       /warehouse_product/{id} [PUT]
// @Summary      Update warehouse_product
// @Description  update warehouse_product
// @Tags         warehouse_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Param        warehouse_product body models.UpdateWarehouseProduct true "warehouse_product"
// @Success      200  {object}  models.WarehouseProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateWarehouseProduct(c *gin.Context) {
	updateWarehouseProduct := &pbw.WarehouseProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateWarehouseProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateWarehouseProduct.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.WarehouseProductService().Update(ctx, updateWarehouseProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating warehouse_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteWarehouseProduct godoc
// @Router       /warehouse_product/{id} [DELETE]
// @Summary      Delete warehouse_product
// @Description  delete warehouse_product
// @Tags         warehouse_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteWarehouseProduct(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.WarehouseProductService().Delete(ctx, &pbw.WarehouseProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting warehouse_product!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
