package handler

import (
	pbo "market_api_gateway/genproto/organization_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateStaff godoc
// @Router       /staff [POST]
// @Summary      Create a new staff
// @Description  Create a new staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        staff body models.CreateStaff false "staff"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateStaff(c *gin.Context) {
	request := pbo.CreateStaffRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.StaffService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating staff", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "staff created!", http.StatusCreated, resp)
}

// GetStaff godoc
// @Router       /staff/{id} [GET]
// @Summary      Gets staff
// @Description  get staff by id
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        id path string true "staff"
// @Success      200  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStaff(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	staff, err := h.services.StaffService().Get(ctx, &pbo.StaffPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting staff by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, staff)
}

// GetStaffList godoc
// @Router       /staffs [GET]
// @Summary      Get staff list
// @Description  get staff list
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.StaffResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStaffList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.StaffService().GetList(ctx, &pbo.GetStaffListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting staffs", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateStaff godoc
// @Router       /staff/{id} [PUT]
// @Summary      Update staff
// @Description  update staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Param        staff body models.UpdateStaff true "staff"
// @Success      200  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStaff(c *gin.Context) {
	updateStaff := &pbo.Staff{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateStaff)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateStaff.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.StaffService().Update(ctx, updateStaff)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating staff", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteStaff godoc
// @Router       /staff/{id} [DELETE]
// @Summary      Delete staff
// @Description  delete staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param 		 id path string true "branch_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteStaff(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.StaffService().Delete(ctx, &pbo.StaffPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting staff!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
