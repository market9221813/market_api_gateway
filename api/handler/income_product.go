package handler

import (
	pbw "market_api_gateway/genproto/warehouse_service_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateIncomeProduct godoc
// @Router       /income_product [POST]
// @Summary      Create a new income_product
// @Description  Create a new income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        income_product body models.CreateIncomeProduct false "income_product"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncomeProduct(c *gin.Context) {
	request := pbw.CreateIncomeProductRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeProductService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating income_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Income Product created!", http.StatusCreated, resp)
}

// GetIncomeProduct godoc
// @Router       /income_product/{id} [GET]
// @Summary      Gets income_product
// @Description  get income_product by id
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        id path string true "income_product"
// @Success      200  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeProduct(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	income_product, err := h.services.IncomeProductService().Get(ctx, &pbw.IncomeProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting income_product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, income_product)
}

// GetIncomeProductList godoc
// @Router       /income_products [GET]
// @Summary      Get income_product list
// @Description  get income_product list
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Success      200  {object}  models.IncomesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeProductList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeProductService().GetList(ctx, &pbw.GetIncomeProductListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
 	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting income_products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateIncomeProduct godoc
// @Router       /income_product/{id} [PUT]
// @Summary      Update income_product
// @Description  update income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Param        income_product body models.UpdateIncomeProduct true "income_product"
// @Success      200  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncomeProduct(c *gin.Context) {
	updateIncomeProduct := &pbw.IncomeProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	err := c.ShouldBindJSON(&updateIncomeProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateIncomeProduct.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.IncomeProductService().Update(ctx, updateIncomeProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating income_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteIncomeProduct godoc
// @Router       /income_product/{id} [DELETE]
// @Summary      Delete income_product
// @Description  delete income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "income_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncomeProduct(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := h.services.IncomeProductService().Delete(ctx, &pbw.IncomeProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting income_product!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, "Deleted successfully!")
}
