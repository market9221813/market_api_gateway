package models

type Provider struct {
	ID        string`json:"id"`
	Name 	  string`json:"name"`
    Phone 	  string`json:"phone"`
    Active    string`json:"active"`
	CreatedAt string`json:"created_at"`
    UpdatedAt string`json:"updated_at"`
}

type CreateProvider struct {
	Name 	  string`json:"name"`
    Phone 	  string`json:"phone"`
    Active    string`json:"active"`
}

type UpdateProvider struct {
	ID        string`json:"id"`
	Name 	  string`json:"name"`
    Phone 	  string`json:"phone"`
    Active    string`json:"active"`
}

type ProvidersResponse struct {
	Providers []Provider`json:"providers"`
	Count          int32`json:"count"`
}