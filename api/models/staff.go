package models

type Staff struct {
	ID        			 string`json:"id"`
	FirstName 			 string`json:"first_name"`
    LastName  			 string`json:"last_name"`
    Phone	  			 string`json:"phone"`
    Login 	  			 string`json:"login"`
    Password  			 string`json:"password"`
    BranchSellingPointId string`json:"branch_selling_point_id"`
    Type      			 string`json:"type"`
	CreatedAt 			 string`json:"created_at"`
    UpdatedAt 			 string`json:"updated_at"`
}

type CreateStaff struct {
	FirstName 			 string`json:"first_name"`
    LastName  			 string`json:"last_name"`
    Phone	  			 string`json:"phone"`
    Login 	  			 string`json:"login"`
    Password  			 string`json:"password"`
    BranchSellingPointId string`json:"branch_selling_point_id"`
    Type      			 string`json:"type"`
}

type UpdateStaff struct {
	ID        			 string`json:"id"`
	FirstName 			 string`json:"first_name"`
    LastName  			 string`json:"last_name"`
    Phone	  			 string`json:"phone"`
    Login 	  			 string`json:"login"`
    Password  			 string`json:"password"`
    BranchSellingPointId string`json:"branch_selling_point_id"`
    Type      			 string`json:"type"`
}

type StaffResponse struct {
	Staff []Staff `json:"staff"`
	Count    int32`json:"count"`
}