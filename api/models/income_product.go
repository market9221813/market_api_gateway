package models

type IncomeProduct struct {
	ID 				 string`json:"id"` 
    CategoryID 		 string`json:"category_id"`  
    ProductName		 string`json:"product_name"`
    Barcode          string`json:"barcode"`
    Quantity 		  int32`json:"quantity"`   
    Price 			float32`json:"price"`
    IncomeID  		 string`json:"income_id"`  
    CreatedAt 		 string`json:"created_at"`  
    UpdatedAt 		 string`json:"updated_at"`  
}

type CreateIncomeProduct struct {
    CategoryID 		 string`json:"category_id"`  
    Barcode          string`json:"barcode"`
    Quantity 		  int32`json:"quantity"`   
    Price 			float32`json:"price"`
    IncomeID  		 string`json:"income_id"`  
 }

type UpdateIncomeProduct struct {
	ID 				 string`json:"id"` 
    CategoryID 		 string`json:"category_id"`  
    Barcode          string`json:"barcode"`
    Quantity 		  int32`json:"quantity"`   
    Price 			float32`json:"price"`
    IncomeID  		 string`json:"income_id"`  
    ProductSellingID string`json:"product_selling_id"`  
}

type IncomeProductsResponse struct {
	IncomeProducts []IncomeProduct`json:"income_products"`
	Count 					 int32`json:"count"`
}