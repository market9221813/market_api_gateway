package models

type Shift struct {
	ID 					 string`json:"id"`
    BranchID 			 string`json:"branch_id"`
    StaffID 			 string`json:"staff_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    OpeningTime 		 string`json:"opening_time"`
    ClosingTime 		 string`json:"closing_time"`
    Status 				 string`json:"status"`
    CreatedAt 			 string`json:"created_at"`
    UpdatedAt  			 string`json:"updated_at"`
}

type CreateShift struct {
	BranchID 			 string`json:"branch_id"`
    StaffID 			 string`json:"staff_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    OpeningTime 		 string`json:"opening_time"`
    ClosingTime 		 string`json:"closing_time"`
    Status 				 string`json:"status"`
}

type UpdateShift struct {
	ID 					 string`json:"id"`
    BranchID 			 string`json:"branch_id"`
    StaffID 			 string`json:"staff_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    OpeningTime 		 string`json:"opening_time"`
    ClosingTime 		 string`json:"closing_time"`
    Status 				 string`json:"status"`
}

type ShiftsResponse struct {
	Shifts []Shift`json:"shifts"` 
	Count 	 int32`json:"count"`
}

type UpdateShiftStatus struct {
    ID 					 string`json:"id"`
    Status 				 string`json:"status"`
}