package models

type Sale struct {
	ID 					 string`json:"id"`
	ShiftID 			 string`json:"shift_id"`
    BranchId 			 string`json:"branch_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    StaffID       		 string`json:"staff_id"`
    Status 				 string`json:"status"`
    PaymentType 		 string`json:"payment_type"`
    TotalSum   			float32`json:"total_sum"`
	CreatedAt 			 string`json:"created_at"`
    UpdatedAt  			 string`json:"updated_at"`
}

type CreateSale struct {
	ShiftID 			 string`json:"shift_id"`
    BranchId 			 string`json:"branch_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    StaffID       		 string`json:"staff_id"`
    Status 				 string`json:"status"`
    PaymentType 		 string`json:"payment_type"`
    TotalSum   			float32`json:"total_sum"`
}

type UpdateSale struct {
	ID 					 string`json:"id"`
	ShiftID 			 string`json:"shift_id"`
    BranchId 			 string`json:"branch_id"`
    BranchSellingPointID string`json:"branch_selling_point_id"`
    StaffID       		 string`json:"staff_id"`
    Status 				 string`json:"status"`
    PaymentType 		 string`json:"payment_type"`
    TotalSum   			float32`json:"total_sum"`
}

type SalesResponse struct {
	Sales []Sale`json:"sales"`
	Count  int32`json:"count"`
}