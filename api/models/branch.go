package models

type Branch struct {
	ID        string`json:"id"`
    Name      string`json:"name"`
    CreatedAt string`json:"created_at"`
    UpdatedAt string`json:"updated_at"`
}

type CreateBranch struct {
	Name      string`json:"name"`
}

type UpdateBranch struct {
	ID        string`json:"id"`
    Name      string`json:"name"`
}

type BranchesResponse struct {
	Branches []Branch`json:"branches"`
	Count       int32`json:"count"`
}

