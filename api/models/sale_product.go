package models

type SaleProduct struct {
	ID 			       string`json:"id"`
	CategoryID 		   string`json:"category_id"`
    ProductID 		   string`json:"product_id"`
    WarehouseProductID string`json:"warehouse_product_id"`
    SaleID 			   string`json:"sale_id"`
    Quantity 			int32`json:"quantity"`
    Price 		  	  float32`json:"price"`
    TotalSum 		  float32`json:"total_sum"`
    CreatedAt 		   string`json:"created_at"`
    UpdatedAt  		   string`json:"updated_at"`
}

type CreateSaleProduct struct {
	CategoryID 		   string`json:"category_id"`
    ProductID 		   string`json:"product_id"`
    WarehouseProductID string`json:"warehouse_product_id"`
    SaleID 			   string`json:"sale_id"`
    Quantity 			int32`json:"quantity"`
    Price 		  	  float32`json:"price"`
    TotalSum 		  float32`json:"total_sum"`
}

type UpdateSaleProduct struct {
	ID 			       string`json:"id"`
	CategoryID 		   string`json:"category_id"`
    ProductID 		   string`json:"product_id"`
    WarehouseProductID string`json:"warehouse_product_id"`
    SaleID 			   string`json:"sale_id"`
    Quantity 			int32`json:"quantity"`
    Price 		  	  float32`json:"price"`
    TotalSum 		  float32`json:"total_sum"`
}

type SaleProductsResponse struct {
	SaleProducts []SaleProduct`json:"sale_products"`
	Count    			 int32`json:"count"`
}