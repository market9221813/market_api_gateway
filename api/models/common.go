package models

type PrimaryKey struct {
	ID string `json:"id"`
}