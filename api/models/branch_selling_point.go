package models

type BranchSellingPoint struct {
	ID        string`json:"id"`
	Address   string`json:"address"`
    Phone     string`json:"phone"`
    BranchId  string`json:"branch_id"`
	CreatedAt string`json:"created_at"`
    UpdatedAt string`json:"updated_at"`
}

type CreateBranchSellingPoint struct {
	Address   string`json:"address"`
    Phone     string`json:"phone"`
    BranchId  string`json:"branch_id"`
}

type UpdateBranchSellingPoint struct {
	ID        string`json:"id"`
	Address   string`json:"address"`
    Phone     string`json:"phone"`
    BranchId  string`json:"branch_id"`
}

type BranchSellingPointsResponse struct {
	BranchSellingPoints []BranchSellingPoint`json:"branch_selling_points"`
	Count 							   int32`json:"count"`
}