package models

type ProductSelling struct {
	ID  	  string`json:"id"`
	Percent  float32`json:"percent"`
	CreatedAt string`json:"created_at"`
	UpdatedAt string`json:"updated_at"`
}

type CreateProductSelling struct {
	Percent  float32`json:"percent"`
}

type UpdateProductSelling struct {
	ID  	  string`json:"id"`
	Percent  float32`json:"percent"`
}

type ProductSellingsResponse struct {
	ProductSellings []ProductSelling`json:"product_sellings"`
	Count 					   int32`json:"count"`
}