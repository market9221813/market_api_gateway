package models

type WarehouseProduct struct {
	ID 					   string`json:"id"`
    BranchID 			   string`json:"branch_id"` 
    BranchSellingProductID string`json:"branch_selling_product_id"` 
    CategoryID 			   string`json:"category_id"` 
    Barcode          string`json:"barcode"`
    IncomeProductPrice    float32`json:"income_product_price"`
    Quantity 			    int32`json:"quantity"`  
    TotalSum 			  float32`json:"total_sum"`   
    CreatedAt 			   string`json:"created_at"`  
    UpdatedAt 			   string`json:"updated_at"`  
}

type CreateWarehouseProduct struct {
	BranchID 			   string`json:"branch_id"` 
    BranchSellingProductID string`json:"branch_selling_product_id"` 
    CategoryID 			   string`json:"category_id"` 
    Barcode                string`json:"barcode"`
    IncomeProductPrice    float32`json:"income_product_price"`
    Quantity 			    int32`json:"quantity"`  
    TotalSum 			  float32`json:"total_sum"`   
}

type UpdateWarehouseProduct struct {
	ID 					   string`json:"id"`
    BranchID 			   string`json:"branch_id"` 
    BranchSellingProductID string`json:"branch_selling_product_id"` 
    CategoryID 			   string`json:"category_id"` 
    Barcode                string`json:"barcode"`
    IncomeProductPrice    float32`json:"income_product_price"`
    Quantity 			    int32`json:"quantity"`  
    TotalSum 			  float32`json:"total_sum"`  
}

type WarehouseProductsResponse struct {
	WarehouseProducts []WarehouseProduct`json:"warehouse_products"`
	Count 						   int32`json:"count"`
}