package models

type Income struct {
	ID 					   string`json:"id"`
    BranchID               string`json:"branch_id"`
    BranchSellingProductID string`json:"branch_selling_point_id"`
    ProviderID 			   string`json:"provider_id"` 
    TotalSum              float32`json:"total_sum"`
    Status 				   string`json:"status"` 
    CreatedAt 			   string`json:"created_at"` 
    UpdatedAt 			   string`json:"updated_at"` 
}

type CreateIncome struct {
    BranchID               string`json:"branch_id"`
	BranchSellingProductID string`json:"branch_selling_point_id"`
    ProviderID 			   string`json:"provider_id"` 
    TotalSum              float32`json:"total_sum"`
    Status 				   string`json:"status"` 
}

type UpdateIncome struct {
	ID 					   string`json:"id"`
    BranchID               string`json:"branch_id"`
    BranchSellingProductID string`json:"branch_selling_point_id"`
    ProviderID 			   string`json:"provider_id"` 
    TotalSum              float32`json:"total_sum"`
    Status 				   string`json:"status"` 
}

type IncomesResponse struct {
	Incomes []Income`json:"incomes"`
	Count      int32`json:"count"`
}