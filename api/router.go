package api

import (
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "market_api_gateway/api/docs"
	"market_api_gateway/api/handler"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

		// Catalog service

		r.POST("/category", h.CreateCategory)
		r.GET("/category/:id", h.GetCategory)
		r.GET("/categories", h.GetCategoryList)
		r.PUT("/category/:id",h.UpdateCategory)
		r.DELETE("/category/:id",h.DeleteCategory)

		r.POST("/product", h.CreateProduct)
		r.GET("/product/:id", h.GetProduct)
		r.GET("/products", h.GetProductList)
		r.PUT("/product/:id",h.UpdateProduct)
		r.DELETE("/product/:id",h.DeleteProduct)

		// Organization service

		r.POST("/branch", h.CreateBranch)
		r.GET("/branch/:id", h.GetBranch)
		r.GET("/branches", h.GetBranchList)
		r.PUT("/branch/:id",h.UpdateBranch)
		r.DELETE("/branch/:id",h.DeleteBranch)

		r.POST("/branch_selling_point", h.CreateBranchSellingPoint)
		r.GET("/branch_selling_point/:id", h.GetBranchSellingPoint)
		r.GET("/branch_selling_points", h.GetBranchSellingPointList)
		r.PUT("/branch_selling_point/:id",h.UpdateBranchSellingPoint)
		r.DELETE("/branch_selling_point/:id",h.DeleteBranchSellingPoint)

		r.POST("/staff", h.CreateStaff)
		r.GET("/staff/:id", h.GetStaff)
		r.GET("/staffs", h.GetStaffList)
		r.PUT("/staff/:id",h.UpdateStaff)
		r.DELETE("/staff/:id",h.DeleteStaff)

		r.POST("/provider", h.CreateProvider)
		r.GET("/provider/:id", h.GetProvider)
		r.GET("/providers", h.GetProviderList)
		r.PUT("/provider/:id",h.UpdateProvider)
		r.DELETE("/provider/:id",h.DeleteProvider)

		r.POST("/staff_login", h.StaffLogin)

		// Warehouse service

		r.POST("/income", h.CreateIncome)
		r.GET("/income/:id", h.GetIncome)
		r.GET("/incomes", h.GetIncomeList)
		r.PUT("/income/:id",h.UpdateIncome)
		r.DELETE("/income/:id",h.DeleteIncome)
		r.POST("/income_logic", h.MakeIncomeLogic)
		r.PATCH("finish_income/:id", h.FinishIncome)

 
		r.POST("/income_product", h.CreateIncomeProduct)
		r.GET("/income_product/:id", h.GetIncomeProduct)
		r.GET("/income_products", h.GetIncomeProductList)
		r.PUT("/income_product/:id",h.UpdateIncomeProduct)
		r.DELETE("/income_product/:id",h.DeleteIncomeProduct)

		r.POST("/product_selling", h.CreateProductSelling)
		r.GET("/product_selling/:id", h.GetProductSelling)
		r.GET("/product_sellings", h.GetProductSellingList)
		r.PUT("/product_selling/:id",h.UpdateProductSelling)
		r.DELETE("/product_selling/:id",h.DeleteProductSelling)

		r.POST("/warehouse_product", h.CreateWarehouseProduct)
		r.GET("/warehouse_product/:id", h.GetWarehouse)
		r.GET("/warehouse_products", h.GetWarehouseList)
		r.PUT("/warehouse_product/:id",h.UpdateWarehouseProduct)
		r.DELETE("/warehouse_product/:id",h.DeleteWarehouseProduct)

		// Cash service

		r.POST("/sale", h.CreateSale)
		r.GET("/sale/:id", h.GetSale)
		r.GET("/sales", h.GetSaleList)
		r.PUT("/sale/:id",h.UpdateSale)
		r.DELETE("/sale/:id",h.DeleteSale)

		r.POST("/sale_product", h.CreateSaleProduct)
		r.GET("/sale_product/:id", h.GetSaleProduct)
		r.GET("/sale_products", h.GetSaleProductList)
		r.PUT("/sale_product/:id",h.UpdateSaleProduct)
		r.DELETE("/sale_product/:id",h.DeleteSaleProduct)

		r.POST("/shift", h.CreateShift)
		r.GET("/shift/:id", h.GetShift)
		r.GET("/shifts", h.GetShiftList)
		r.PUT("/shift/:id",h.UpdateShift)
		r.DELETE("/shift/:id",h.DeleteShift)
		r.POST("/open_shift", h.OpenShiftLogic)
		r.PATCH("/close_shift/:id", h.CloseShift)

		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		return r
}
