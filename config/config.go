package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"os"
)

type Config struct {
	ServiceName string
	Environment string

	HTTPPort string

	CatalogGrpcServiceHost string
	CatalogGrpcServicePort string
	OrganizationGrpcServiceHost string
	OrganizationGrpcServicePort string
	WarehouseGrpcServiceHost string
	WarehouseGrpcServicePort string
	CashGrpcServiceHost string
	CashGrpcServicePort string

}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error!!!", err)
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "service_name"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.CatalogGrpcServiceHost = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_HOST", "localhost"))
	cfg.CatalogGrpcServicePort = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_PORT", ":8080"))
	cfg.OrganizationGrpcServiceHost = cast.ToString(getOrReturnDefault("ORGANIZATION_GRPC_SERVICE_HOST", "localhost"))
	cfg.OrganizationGrpcServicePort = cast.ToString(getOrReturnDefault("ORGANIZATION_GRPC_SERVICE_PORT", ":8080"))
	cfg.WarehouseGrpcServiceHost = cast.ToString(getOrReturnDefault("WAREHOUSE_GRPC_SERVICE_HOST", "localhost"))
	cfg.WarehouseGrpcServicePort = cast.ToString(getOrReturnDefault("WAREHOUSE_GRPC_SERVICE_PORT", ":8080"))
	cfg.CashGrpcServiceHost = cast.ToString(getOrReturnDefault("CASH_GRPC_SERVICE_HOST", "localhost"))
	cfg.CashGrpcServicePort = cast.ToString(getOrReturnDefault("CASH_GRPC_SERVICE_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
