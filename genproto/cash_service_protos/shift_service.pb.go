// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v3.12.4
// source: shift_service.proto

package cash_service

import (
	empty "github.com/golang/protobuf/ptypes/empty"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CreateShiftRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                   string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	BranchId             string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	StaffId              string `protobuf:"bytes,3,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchSellingPointId string `protobuf:"bytes,4,opt,name=branch_selling_point_id,json=branchSellingPointId,proto3" json:"branch_selling_point_id,omitempty"`
	OpeningTime          string `protobuf:"bytes,5,opt,name=opening_time,json=openingTime,proto3" json:"opening_time,omitempty"`
	ClosingTime          string `protobuf:"bytes,6,opt,name=closing_time,json=closingTime,proto3" json:"closing_time,omitempty"`
	Status               string `protobuf:"bytes,7,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *CreateShiftRequest) Reset() {
	*x = CreateShiftRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_shift_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateShiftRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateShiftRequest) ProtoMessage() {}

func (x *CreateShiftRequest) ProtoReflect() protoreflect.Message {
	mi := &file_shift_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateShiftRequest.ProtoReflect.Descriptor instead.
func (*CreateShiftRequest) Descriptor() ([]byte, []int) {
	return file_shift_service_proto_rawDescGZIP(), []int{0}
}

func (x *CreateShiftRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateShiftRequest) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *CreateShiftRequest) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *CreateShiftRequest) GetBranchSellingPointId() string {
	if x != nil {
		return x.BranchSellingPointId
	}
	return ""
}

func (x *CreateShiftRequest) GetOpeningTime() string {
	if x != nil {
		return x.OpeningTime
	}
	return ""
}

func (x *CreateShiftRequest) GetClosingTime() string {
	if x != nil {
		return x.ClosingTime
	}
	return ""
}

func (x *CreateShiftRequest) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type Shift struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id                   string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	BranchId             string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	StaffId              string `protobuf:"bytes,3,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchSellingPointId string `protobuf:"bytes,4,opt,name=branch_selling_point_id,json=branchSellingPointId,proto3" json:"branch_selling_point_id,omitempty"`
	OpeningTime          string `protobuf:"bytes,5,opt,name=opening_time,json=openingTime,proto3" json:"opening_time,omitempty"`
	ClosingTime          string `protobuf:"bytes,6,opt,name=closing_time,json=closingTime,proto3" json:"closing_time,omitempty"`
	Status               string `protobuf:"bytes,7,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt            string `protobuf:"bytes,8,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string `protobuf:"bytes,9,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Shift) Reset() {
	*x = Shift{}
	if protoimpl.UnsafeEnabled {
		mi := &file_shift_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Shift) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Shift) ProtoMessage() {}

func (x *Shift) ProtoReflect() protoreflect.Message {
	mi := &file_shift_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Shift.ProtoReflect.Descriptor instead.
func (*Shift) Descriptor() ([]byte, []int) {
	return file_shift_service_proto_rawDescGZIP(), []int{1}
}

func (x *Shift) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Shift) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Shift) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *Shift) GetBranchSellingPointId() string {
	if x != nil {
		return x.BranchSellingPointId
	}
	return ""
}

func (x *Shift) GetOpeningTime() string {
	if x != nil {
		return x.OpeningTime
	}
	return ""
}

func (x *Shift) GetClosingTime() string {
	if x != nil {
		return x.ClosingTime
	}
	return ""
}

func (x *Shift) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *Shift) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Shift) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type ShiftPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ShiftPrimaryKey) Reset() {
	*x = ShiftPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_shift_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ShiftPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ShiftPrimaryKey) ProtoMessage() {}

func (x *ShiftPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_shift_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ShiftPrimaryKey.ProtoReflect.Descriptor instead.
func (*ShiftPrimaryKey) Descriptor() ([]byte, []int) {
	return file_shift_service_proto_rawDescGZIP(), []int{2}
}

func (x *ShiftPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetShiftListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page   int32  `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Limit  int32  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetShiftListRequest) Reset() {
	*x = GetShiftListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_shift_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetShiftListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetShiftListRequest) ProtoMessage() {}

func (x *GetShiftListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_shift_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetShiftListRequest.ProtoReflect.Descriptor instead.
func (*GetShiftListRequest) Descriptor() ([]byte, []int) {
	return file_shift_service_proto_rawDescGZIP(), []int{3}
}

func (x *GetShiftListRequest) GetPage() int32 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetShiftListRequest) GetLimit() int32 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetShiftListRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type ShiftsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Shifts []*Shift `protobuf:"bytes,1,rep,name=shifts,proto3" json:"shifts,omitempty"`
	Count  int32    `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *ShiftsResponse) Reset() {
	*x = ShiftsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_shift_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ShiftsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ShiftsResponse) ProtoMessage() {}

func (x *ShiftsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_shift_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ShiftsResponse.ProtoReflect.Descriptor instead.
func (*ShiftsResponse) Descriptor() ([]byte, []int) {
	return file_shift_service_proto_rawDescGZIP(), []int{4}
}

func (x *ShiftsResponse) GetShifts() []*Shift {
	if x != nil {
		return x.Shifts
	}
	return nil
}

func (x *ShiftsResponse) GetCount() int32 {
	if x != nil {
		return x.Count
	}
	return 0
}

var File_shift_service_proto protoreflect.FileDescriptor

var file_shift_service_proto_rawDesc = []byte{
	0x0a, 0x13, 0x73, 0x68, 0x69, 0x66, 0x74, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x22, 0xf1, 0x01, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x68, 0x69, 0x66, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63,
	0x68, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e,
	0x63, 0x68, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f, 0x69, 0x64,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49, 0x64, 0x12,
	0x35, 0x0a, 0x17, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x73, 0x65, 0x6c, 0x6c, 0x69, 0x6e,
	0x67, 0x5f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x14, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x53, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x50,
	0x6f, 0x69, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x6f, 0x70, 0x65, 0x6e, 0x69, 0x6e,
	0x67, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x6f, 0x70,
	0x65, 0x6e, 0x69, 0x6e, 0x67, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6c, 0x6f,
	0x73, 0x69, 0x6e, 0x67, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x63, 0x6c, 0x6f, 0x73, 0x69, 0x6e, 0x67, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x16, 0x0a, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0xa2, 0x02, 0x0a, 0x05, 0x53, 0x68, 0x69, 0x66, 0x74, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1b,
	0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73,
	0x74, 0x61, 0x66, 0x66, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73,
	0x74, 0x61, 0x66, 0x66, 0x49, 0x64, 0x12, 0x35, 0x0a, 0x17, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68,
	0x5f, 0x73, 0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x5f, 0x69,
	0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x14, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x53,
	0x65, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x21, 0x0a,
	0x0c, 0x6f, 0x70, 0x65, 0x6e, 0x69, 0x6e, 0x67, 0x5f, 0x74, 0x69, 0x6d, 0x65, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0b, 0x6f, 0x70, 0x65, 0x6e, 0x69, 0x6e, 0x67, 0x54, 0x69, 0x6d, 0x65,
	0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6c, 0x6f, 0x73, 0x69, 0x6e, 0x67, 0x5f, 0x74, 0x69, 0x6d, 0x65,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63, 0x6c, 0x6f, 0x73, 0x69, 0x6e, 0x67, 0x54,
	0x69, 0x6d, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x63,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x21, 0x0a, 0x0f, 0x53, 0x68, 0x69,
	0x66, 0x74, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x57, 0x0a, 0x13,
	0x47, 0x65, 0x74, 0x53, 0x68, 0x69, 0x66, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a,
	0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x53, 0x0a, 0x0e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2b, 0x0a, 0x06, 0x73, 0x68, 0x69, 0x66, 0x74,
	0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x52, 0x06, 0x73, 0x68,
	0x69, 0x66, 0x74, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x32, 0xe2, 0x03, 0x0a, 0x0c, 0x53,
	0x68, 0x69, 0x66, 0x74, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x41, 0x0a, 0x06, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x20, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x68, 0x69, 0x66, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x22, 0x00, 0x12, 0x3b,
	0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x1d, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72,
	0x79, 0x4b, 0x65, 0x79, 0x1a, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x22, 0x00, 0x12, 0x4c, 0x0a, 0x07, 0x47,
	0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x21, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x65, 0x74, 0x53, 0x68, 0x69, 0x66, 0x74, 0x4c, 0x69,
	0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1c, 0x2e, 0x63, 0x61, 0x73, 0x68,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x34, 0x0a, 0x06, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x12, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x1a, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x22, 0x00, 0x12,
	0x41, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x1d, 0x2e, 0x63, 0x61, 0x73, 0x68,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x50, 0x72,
	0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x22, 0x00, 0x12, 0x44, 0x0a, 0x09, 0x4f, 0x70, 0x65, 0x6e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x12,
	0x20, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x68, 0x69, 0x66, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x13, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x22, 0x00, 0x12, 0x45, 0x0a, 0x0a, 0x43, 0x6c, 0x6f, 0x73,
	0x65, 0x53, 0x68, 0x69, 0x66, 0x74, 0x12, 0x1d, 0x2e, 0x63, 0x61, 0x73, 0x68, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53, 0x68, 0x69, 0x66, 0x74, 0x50, 0x72, 0x69, 0x6d, 0x61,
	0x72, 0x79, 0x4b, 0x65, 0x79, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42,
	0x17, 0x5a, 0x15, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63, 0x61, 0x73, 0x68,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_shift_service_proto_rawDescOnce sync.Once
	file_shift_service_proto_rawDescData = file_shift_service_proto_rawDesc
)

func file_shift_service_proto_rawDescGZIP() []byte {
	file_shift_service_proto_rawDescOnce.Do(func() {
		file_shift_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_shift_service_proto_rawDescData)
	})
	return file_shift_service_proto_rawDescData
}

var file_shift_service_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_shift_service_proto_goTypes = []interface{}{
	(*CreateShiftRequest)(nil),  // 0: cash_service.CreateShiftRequest
	(*Shift)(nil),               // 1: cash_service.Shift
	(*ShiftPrimaryKey)(nil),     // 2: cash_service.ShiftPrimaryKey
	(*GetShiftListRequest)(nil), // 3: cash_service.GetShiftListRequest
	(*ShiftsResponse)(nil),      // 4: cash_service.ShiftsResponse
	(*empty.Empty)(nil),         // 5: google.protobuf.Empty
}
var file_shift_service_proto_depIdxs = []int32{
	1, // 0: cash_service.ShiftsResponse.shifts:type_name -> cash_service.Shift
	0, // 1: cash_service.ShiftService.Create:input_type -> cash_service.CreateShiftRequest
	2, // 2: cash_service.ShiftService.Get:input_type -> cash_service.ShiftPrimaryKey
	3, // 3: cash_service.ShiftService.GetList:input_type -> cash_service.GetShiftListRequest
	1, // 4: cash_service.ShiftService.Update:input_type -> cash_service.Shift
	2, // 5: cash_service.ShiftService.Delete:input_type -> cash_service.ShiftPrimaryKey
	0, // 6: cash_service.ShiftService.OpenShift:input_type -> cash_service.CreateShiftRequest
	2, // 7: cash_service.ShiftService.CloseShift:input_type -> cash_service.ShiftPrimaryKey
	1, // 8: cash_service.ShiftService.Create:output_type -> cash_service.Shift
	1, // 9: cash_service.ShiftService.Get:output_type -> cash_service.Shift
	4, // 10: cash_service.ShiftService.GetList:output_type -> cash_service.ShiftsResponse
	1, // 11: cash_service.ShiftService.Update:output_type -> cash_service.Shift
	5, // 12: cash_service.ShiftService.Delete:output_type -> google.protobuf.Empty
	1, // 13: cash_service.ShiftService.OpenShift:output_type -> cash_service.Shift
	5, // 14: cash_service.ShiftService.CloseShift:output_type -> google.protobuf.Empty
	8, // [8:15] is the sub-list for method output_type
	1, // [1:8] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_shift_service_proto_init() }
func file_shift_service_proto_init() {
	if File_shift_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_shift_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateShiftRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_shift_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Shift); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_shift_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ShiftPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_shift_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetShiftListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_shift_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ShiftsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_shift_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_shift_service_proto_goTypes,
		DependencyIndexes: file_shift_service_proto_depIdxs,
		MessageInfos:      file_shift_service_proto_msgTypes,
	}.Build()
	File_shift_service_proto = out.File
	file_shift_service_proto_rawDesc = nil
	file_shift_service_proto_goTypes = nil
	file_shift_service_proto_depIdxs = nil
}
