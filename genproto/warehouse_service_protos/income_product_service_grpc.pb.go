// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.12.4
// source: income_product_service.proto

package warehouse_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	IncomeProductService_MakeIncome_FullMethodName = "/warehouse_service.IncomeProductService/MakeIncome"
	IncomeProductService_Create_FullMethodName     = "/warehouse_service.IncomeProductService/Create"
	IncomeProductService_Get_FullMethodName        = "/warehouse_service.IncomeProductService/Get"
	IncomeProductService_GetList_FullMethodName    = "/warehouse_service.IncomeProductService/GetList"
	IncomeProductService_Update_FullMethodName     = "/warehouse_service.IncomeProductService/Update"
	IncomeProductService_Delete_FullMethodName     = "/warehouse_service.IncomeProductService/Delete"
)

// IncomeProductServiceClient is the client API for IncomeProductService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type IncomeProductServiceClient interface {
	MakeIncome(ctx context.Context, in *CreateIncomeProductRequest, opts ...grpc.CallOption) (*IncomeProduct, error)
	Create(ctx context.Context, in *CreateIncomeProductRequest, opts ...grpc.CallOption) (*IncomeProduct, error)
	Get(ctx context.Context, in *IncomeProductPrimaryKey, opts ...grpc.CallOption) (*IncomeProduct, error)
	GetList(ctx context.Context, in *GetIncomeProductListRequest, opts ...grpc.CallOption) (*IncomeProductsResponse, error)
	Update(ctx context.Context, in *IncomeProduct, opts ...grpc.CallOption) (*IncomeProduct, error)
	Delete(ctx context.Context, in *IncomeProductPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type incomeProductServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewIncomeProductServiceClient(cc grpc.ClientConnInterface) IncomeProductServiceClient {
	return &incomeProductServiceClient{cc}
}

func (c *incomeProductServiceClient) MakeIncome(ctx context.Context, in *CreateIncomeProductRequest, opts ...grpc.CallOption) (*IncomeProduct, error) {
	out := new(IncomeProduct)
	err := c.cc.Invoke(ctx, IncomeProductService_MakeIncome_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *incomeProductServiceClient) Create(ctx context.Context, in *CreateIncomeProductRequest, opts ...grpc.CallOption) (*IncomeProduct, error) {
	out := new(IncomeProduct)
	err := c.cc.Invoke(ctx, IncomeProductService_Create_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *incomeProductServiceClient) Get(ctx context.Context, in *IncomeProductPrimaryKey, opts ...grpc.CallOption) (*IncomeProduct, error) {
	out := new(IncomeProduct)
	err := c.cc.Invoke(ctx, IncomeProductService_Get_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *incomeProductServiceClient) GetList(ctx context.Context, in *GetIncomeProductListRequest, opts ...grpc.CallOption) (*IncomeProductsResponse, error) {
	out := new(IncomeProductsResponse)
	err := c.cc.Invoke(ctx, IncomeProductService_GetList_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *incomeProductServiceClient) Update(ctx context.Context, in *IncomeProduct, opts ...grpc.CallOption) (*IncomeProduct, error) {
	out := new(IncomeProduct)
	err := c.cc.Invoke(ctx, IncomeProductService_Update_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *incomeProductServiceClient) Delete(ctx context.Context, in *IncomeProductPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, IncomeProductService_Delete_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// IncomeProductServiceServer is the server API for IncomeProductService service.
// All implementations must embed UnimplementedIncomeProductServiceServer
// for forward compatibility
type IncomeProductServiceServer interface {
	MakeIncome(context.Context, *CreateIncomeProductRequest) (*IncomeProduct, error)
	Create(context.Context, *CreateIncomeProductRequest) (*IncomeProduct, error)
	Get(context.Context, *IncomeProductPrimaryKey) (*IncomeProduct, error)
	GetList(context.Context, *GetIncomeProductListRequest) (*IncomeProductsResponse, error)
	Update(context.Context, *IncomeProduct) (*IncomeProduct, error)
	Delete(context.Context, *IncomeProductPrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedIncomeProductServiceServer()
}

// UnimplementedIncomeProductServiceServer must be embedded to have forward compatible implementations.
type UnimplementedIncomeProductServiceServer struct {
}

func (UnimplementedIncomeProductServiceServer) MakeIncome(context.Context, *CreateIncomeProductRequest) (*IncomeProduct, error) {
	return nil, status.Errorf(codes.Unimplemented, "method MakeIncome not implemented")
}
func (UnimplementedIncomeProductServiceServer) Create(context.Context, *CreateIncomeProductRequest) (*IncomeProduct, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedIncomeProductServiceServer) Get(context.Context, *IncomeProductPrimaryKey) (*IncomeProduct, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedIncomeProductServiceServer) GetList(context.Context, *GetIncomeProductListRequest) (*IncomeProductsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedIncomeProductServiceServer) Update(context.Context, *IncomeProduct) (*IncomeProduct, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedIncomeProductServiceServer) Delete(context.Context, *IncomeProductPrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedIncomeProductServiceServer) mustEmbedUnimplementedIncomeProductServiceServer() {}

// UnsafeIncomeProductServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to IncomeProductServiceServer will
// result in compilation errors.
type UnsafeIncomeProductServiceServer interface {
	mustEmbedUnimplementedIncomeProductServiceServer()
}

func RegisterIncomeProductServiceServer(s grpc.ServiceRegistrar, srv IncomeProductServiceServer) {
	s.RegisterService(&IncomeProductService_ServiceDesc, srv)
}

func _IncomeProductService_MakeIncome_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateIncomeProductRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).MakeIncome(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_MakeIncome_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).MakeIncome(ctx, req.(*CreateIncomeProductRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IncomeProductService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateIncomeProductRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).Create(ctx, req.(*CreateIncomeProductRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IncomeProductService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IncomeProductPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_Get_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).Get(ctx, req.(*IncomeProductPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _IncomeProductService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetIncomeProductListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_GetList_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).GetList(ctx, req.(*GetIncomeProductListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _IncomeProductService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IncomeProduct)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).Update(ctx, req.(*IncomeProduct))
	}
	return interceptor(ctx, in, info, handler)
}

func _IncomeProductService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IncomeProductPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IncomeProductServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: IncomeProductService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IncomeProductServiceServer).Delete(ctx, req.(*IncomeProductPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// IncomeProductService_ServiceDesc is the grpc.ServiceDesc for IncomeProductService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var IncomeProductService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "warehouse_service.IncomeProductService",
	HandlerType: (*IncomeProductServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "MakeIncome",
			Handler:    _IncomeProductService_MakeIncome_Handler,
		},
		{
			MethodName: "Create",
			Handler:    _IncomeProductService_Create_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _IncomeProductService_Get_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _IncomeProductService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _IncomeProductService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _IncomeProductService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "income_product_service.proto",
}
